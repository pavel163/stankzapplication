package com.ebr163.rsk.stankzapplication.adapter.exclusive;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebr163.rsk.stankzapplication.R;
import com.ebr163.rsk.stankzapplication.adapter.news.FooterViewHolder;
import com.ebr163.rsk.stankzapplication.adapter.news.NewsViewHolder;
import com.ebr163.rsk.stankzapplication.model.NewModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Bakht on 17.11.2015.
 */
public class ExclusiveAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_FOOTER = 0;
    private static final int TYPE_ITEM = 1;

    public static List<NewModel> data;

    public ExclusiveAdapter(List<NewModel> data){
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        if(viewType == TYPE_FOOTER) {
            View v = LayoutInflater.from (parent.getContext ()).inflate (R.layout.footer_layout, parent, false);
            return FooterViewHolder.newInstance(v, 1);
        }else {
            View view = LayoutInflater.from(context).inflate(R.layout.item_new, parent, false);
            return NewsViewHolder.newInstance(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (isFooter(position)) {
            FooterViewHolder holder = (FooterViewHolder) viewHolder;
            holder.title1.setText(data.get(0).title);
            holder.title2.setText(data.get(1).title);
            holder.title3.setText(data.get(2).title);
            holder.title4.setText(data.get(3).title);

            Picasso.with( holder.image1.getContext())
                    .load(data.get(0).imageLink)
                    .placeholder(R.drawable.no_image)
                    .into(holder.image1);
            Picasso.with( holder.image2.getContext())
                    .load(data.get(1).imageLink)
                    .placeholder(R.drawable.no_image)
                    .into(holder.image2);
            Picasso.with( holder.image3.getContext())
                    .load(data.get(2).imageLink)
                    .placeholder(R.drawable.no_image)
                    .into(holder.image3);
            Picasso.with( holder.image4.getContext())
                    .load(data.get(3).imageLink)
                    .placeholder(R.drawable.no_image)
                    .into(holder.image4);

            holder.pubdata1.setText(data.get(0).date);
            holder.pubdata2.setText(data.get(1).date);
            holder.pubdata3.setText(data.get(2).date);
            holder.pubdata4.setText(data.get(3).date);
        }else {
            NewsViewHolder holder = (NewsViewHolder) viewHolder;
            holder.setTitle(data.get(position).title);
            holder.setDescription(data.get(position).description);
            holder.setImage(data.get(position).imageLink);
            holder.setLink(data.get(position).link);
            holder.setH2(data.get(position).h2);
            holder.setPubdata(data.get(position).date);
            holder.setContent(data.get(position).content);
        }
    }


    public boolean isHeader(int position) {
        return position == 0;
    }

    public boolean isFooter(int position) {
        return position == data.size() - 1;
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public int getItemViewType (int position) {
        if(isFooter (position)) {
            return TYPE_FOOTER;
        }else {
            return TYPE_ITEM;
        }
    }
}
