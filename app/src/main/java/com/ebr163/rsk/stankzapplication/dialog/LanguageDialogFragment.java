package com.ebr163.rsk.stankzapplication.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;

import com.ebr163.rsk.stankzapplication.MainActivity;
import com.ebr163.rsk.stankzapplication.R;

import java.util.Locale;

/**
 * Created by pavel on 02.12.15.
 */
public class LanguageDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {

    private String data[];
    private Locale myLocale;
    private String lang;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        data = new String[2];
        data[0] = getResources().getString(R.string.language_ru);
        data[1] = getResources().getString(R.string.language_kk);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getActivity().getString(R.string.language))
                .setSingleChoiceItems(data, -1, this)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setLocale(lang);
                    }
                });
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if(which == 0){
            lang = "ru";
        }else{
            lang = "kk";
        }
    }

    public void setLocale(String lang) {
        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(getActivity(), MainActivity.class);
        startActivity(refresh);
    }
}
