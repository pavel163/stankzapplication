package com.ebr163.rsk.stankzapplication.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by Bakht on 03.12.2015.
 */
public class ApplicationHelper {

    public static final String NOTIFICATION_ON = "on";
    public static final String NOTIFICATION_OFF = "off";
    public static final String NOTIFICATION = "notification";
    public static final String SETTINGS = "settings";

    public static void saveNotificationStatus(Context context, String status) {
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(NOTIFICATION, status);
        Log.d(NOTIFICATION, status);
        editor.apply();
    }

    public static String getNotificationStatus(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        return preferences.getString(NOTIFICATION, "off");
    }

}
