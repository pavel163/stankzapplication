package com.ebr163.rsk.stankzapplication;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.ebr163.rsk.stankzapplication.adapter.PagerAdapter;
import com.ebr163.rsk.stankzapplication.base.BaseWithFragmentActivity;
import com.ebr163.rsk.stankzapplication.base.ConstClass;
import com.ebr163.rsk.stankzapplication.dialog.LanguageDialogFragment;
import com.ebr163.rsk.stankzapplication.dialog.NotificationDialog;
import com.ebr163.rsk.stankzapplication.fragment.ExclusiveFragment;
import com.ebr163.rsk.stankzapplication.fragment.MainFragment;

public class MainActivity extends BaseWithFragmentActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private static final String TAG_MAIN = "tagmain";
    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ActionBarDrawerToggle actionBarDrawerToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();
        setupViewPager();
        setupTabLayout();
    }

    private void initUI(){
        drawerLayout = (DrawerLayout) findViewById(R.id.main_drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.main_navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        initToolbar();
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        toolbarTitle.setText("STAN.KZ");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                R.string.action_settings,
                R.string.action_settings);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    private void setupViewPager(){
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        pagerAdapter = new PagerAdapter(fragmentManager);
        pagerAdapter.addFragment(MainFragment.newInstance(), getResources().getString(R.string.tab_news));
        pagerAdapter.addFragment(ExclusiveFragment.newInstance(), getResources().getString(R.string.tab_exclusive));
        viewPager.setAdapter(pagerAdapter);
        setupTabLayout();
    }

    /*Инициализация табов*/
    private void setupTabLayout(){
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()){
          /*  case R.id.item_navigation_2:
                LanguageDialogFragment dialog = new LanguageDialogFragment();
                dialog.show(fragmentManager, dialog.getClass().getName());
                break;*/
            case R.id.item_navigation_3:
                NotificationDialog notificationDialog = new NotificationDialog();
                notificationDialog.show(fragmentManager, notificationDialog.getClass().getName());
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        Uri address = null;
        switch (v.getId()){
            case R.id.twitter:
                address = Uri.parse(ConstClass.TWITTER);
                intent = new Intent(Intent.ACTION_VIEW, address);
                break;
            case R.id.mail:
                intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ConstClass.MAIL});
                break;
            case R.id.facebook:
                address = Uri.parse(ConstClass.FACEBOOK);
                intent = new Intent(Intent.ACTION_VIEW, address);
                break;
            case R.id.vk:
                address = Uri.parse(ConstClass.VK);
                intent = new Intent(Intent.ACTION_VIEW, address);
                break;
        }
        startActivity(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        RssService.getInstance().removeDoc();
    }
}
