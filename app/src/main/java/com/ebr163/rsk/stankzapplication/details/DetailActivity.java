package com.ebr163.rsk.stankzapplication.details;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.ebr163.rsk.stankzapplication.R;
import com.ebr163.rsk.stankzapplication.base.BaseFragment;
import com.ebr163.rsk.stankzapplication.base.BaseWithFragmentActivity;
import com.ebr163.rsk.stankzapplication.fragment.DetailFragment;
import com.ebr163.rsk.stankzapplication.model.NewModel;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.util.ArrayList;
import java.util.List;


public class DetailActivity extends BaseWithFragmentActivity implements View.OnClickListener {

    private String[] vkScope = {VKScope.WALL, VKScope.PAGES};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        initToolbar();
        fragment = (BaseFragment) fragmentManager.findFragmentById(R.id.fragment);
        ((DetailFragment) fragment).setNewModel((NewModel) getIntent().getSerializableExtra("news"));
        List<NewModel> list = new ArrayList<>();
        list.add((NewModel) getIntent().getSerializableExtra("news1"));
        list.add((NewModel) getIntent().getSerializableExtra("news2"));
        list.add((NewModel) getIntent().getSerializableExtra("news3"));
        list.add((NewModel) getIntent().getSerializableExtra("news4"));
        ((DetailFragment) fragment).setFooter(list);

    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        toolbarTitle.setText(getResources().getString(R.string.tab_news));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.vk:
                sendNewsInVK();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                VKRequest request = VKApi.wall().post(VKParameters.from(VKApiConst.MESSAGE, "Привет, друзья!"));
                request.attempts = 1;
                request.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        super.onComplete(response);
                        Toast.makeText(DetailActivity.this, "Опубликовано", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onError(VKError error) {
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void sendNewsInVK() {
        VKSdk.login(this, vkScope);

    }
}
