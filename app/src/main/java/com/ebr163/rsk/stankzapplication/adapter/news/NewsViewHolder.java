package com.ebr163.rsk.stankzapplication.adapter.news;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebr163.rsk.stankzapplication.R;
import com.ebr163.rsk.stankzapplication.details.DetailActivity;
import com.ebr163.rsk.stankzapplication.model.NewModel;
import com.squareup.picasso.Picasso;

/**
 * Created by Bakht on 17.11.2015.
 */
public class NewsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView title;
    private TextView description;
    private ImageView image;
    private TextView link;
    private TextView h2;
    private TextView pubdata;
    private TextView content;

    public NewsViewHolder(View itemView, TextView title, TextView description, ImageView image, TextView link, TextView h2, TextView pubdata,
                          TextView content) {
        super(itemView);
        itemView.setOnClickListener(this);

        this.title = title;
        this.description = description;
        this.image = image;
        this.link = link;
        this.h2 = h2;
        this.pubdata = pubdata;
        this.content = content;
    }

    public static NewsViewHolder newInstance(View parent) {
        TextView title = (TextView) parent.findViewById(R.id.title);
        TextView description = (TextView) parent.findViewById(R.id.description);
        ImageView image = (ImageView) parent.findViewById(R.id.image);
        TextView link = (TextView) parent.findViewById(R.id.link);
        TextView h2 = (TextView) parent.findViewById(R.id.h2);
        TextView pubdata = (TextView) parent.findViewById(R.id.pubdata);
        TextView content = (TextView) parent.findViewById(R.id.content);
        return new NewsViewHolder(parent, title, description, image, link, h2, pubdata, content);
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void setDescription(String description) {
        this.description.setText(description);
    }

    public void setImage(String imageLink) {
        image.setTag(imageLink);
        Picasso.with(image.getContext())
                .load(imageLink)
                .placeholder(R.drawable.no_image)
                .into(image);
    }

    public void setLink(String link) {
        this.link.setText(link);
    }

    public void setH2(String h2){
        this.h2.setText(h2);
    }

    public void setPubdata(String pubdata){
        this.pubdata.setText(pubdata);
    }

    public void setContent(String content){
        this.content.setText(content);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(v.getContext(), DetailActivity.class);
        NewModel newModel = new NewModel();
        newModel.title = title.getText().toString();
        newModel.description = description.getText().toString();
        newModel.imageLink = image.getTag().toString();
        newModel.link = link.getText().toString();
        newModel.h2 = h2.getText().toString();
        newModel.date = pubdata.getText().toString();
        newModel.content = content.getText().toString();
        intent.putExtra("news", newModel);
        intent.putExtra("news1", NewsAdapter.data.get(0));
        intent.putExtra("news2", NewsAdapter.data.get(1));
        intent.putExtra("news3", NewsAdapter.data.get(2));
        intent.putExtra("news4", NewsAdapter.data.get(3));
        v.getContext().startActivity(intent);
    }

    public void setVisibleTitle(){
        title.setVisibility(View.GONE);
        image.setVisibility(View.GONE);
    }
}
