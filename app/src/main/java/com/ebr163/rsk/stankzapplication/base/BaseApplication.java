package com.ebr163.rsk.stankzapplication.base;

import android.app.Application;
import android.os.SystemClock;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKSdk;

import io.fabric.sdk.android.Fabric;
import java.util.concurrent.TimeUnit;

/**
 * Created by Bakht on 16.11.2015.
 */
public class BaseApplication extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "nnT20ZEmGTvgqvF2meh7mJc3H";
    private static final String TWITTER_SECRET = "6KiZVYEjClIHesuUSP5vQXdWNLXRvaaT1DhMYkkSmtVPLlei4m";


    VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
            if (newToken == null) {

            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        vkAccessTokenTracker.startTracking();
        VKSdk.initialize(this);

    }
}
