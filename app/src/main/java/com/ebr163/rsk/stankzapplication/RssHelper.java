package com.ebr163.rsk.stankzapplication;

/**
 * Created by pavel on 02.12.15.
 */
public class RssHelper {

    public static String clearText(String text){
        while(text.contains("<") && text.contains(">")){
            String dop;
            if (text.indexOf("<") >= text.indexOf(">")){
                dop = text.substring(0, text.indexOf(">")+1);

            }else {
                dop = text.substring(text.indexOf("<"), text.indexOf(">")+1);
            }
            text = text.replace(dop, "");
        }
        while (text.contains("&nbsp;")){
            text = text.replace("&nbsp;", "");
        }

        return  text.replaceAll("&#[0-9]*;" ,"");
    }

    public static String clearImg (String text){
        return text.replaceAll("<img.*/>", "");
    }

    public static String clearHref (String text){
        return text.replaceAll("<a.*</a>", "");
    }

    public static String reformatDataandTime(String data){
        StringBuilder result = new StringBuilder();
        String[] splitData = data.split(" ");
        result.append(splitData[1]+" ");
        switch (splitData[2]){
            case "Dec":
                result.append("декабря");
                break;
            case "Jan":
                result.append("января");
                break;
            case "Feb":
                result.append("февраля");
                break;
            case "Mar":
                result.append("марта");
                break;
            case "May":
                result.append("мая");
                break;
            case "Jun":
                result.append("июня");
                break;
            case "Aug":
                result.append("августа");
                break;
            case "Sep":
                result.append("сентября");
                break;
            case "Oct":
                result.append("октября");
                break;
            case "Nov":
                result.append("ноября");
                break;
            case "Jul":
                result.append("июля");
                break;
        }

        result.append(" ");

        result.append(splitData[4]);

        return result.toString();
    }
}
