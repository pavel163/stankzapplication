package com.ebr163.rsk.stankzapplication.model;

import java.io.Serializable;

/**
 * Created by Bakht on 21.11.2015.
 */
public class NewModel implements Serializable {
    public String title;
    public String description;
    public String imageLink;
    public String link;
    public String date;
    public String content;
    public String h2;
}
