package com.ebr163.rsk.stankzapplication.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ebr163.rsk.stankzapplication.R;
import com.ebr163.rsk.stankzapplication.RssService;
import com.ebr163.rsk.stankzapplication.adapter.news.NewsAdapter;
import com.ebr163.rsk.stankzapplication.adapter.news.NewsViewHolder;
import com.ebr163.rsk.stankzapplication.adapter.news.RecyclerItemClickListener;
import com.ebr163.rsk.stankzapplication.base.BaseFragment;
import com.ebr163.rsk.stankzapplication.details.DetailActivity;
import com.ebr163.rsk.stankzapplication.model.NewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by Bakht on 21.11.2015.
 */
public class MainFragment extends BaseFragment{

    private NewsAdapter adapter;
    private RecyclerView recyclerView;
    private ImageView imageView;
    private TextView titleText;
    private RssService rssService;
    private List<NewModel> newModels;
    private List<NewModel> newModels2;
    protected ProgressDialog progressDialog;

    public static MainFragment newInstance() {
        MainFragment mainFragment = new MainFragment();
        return mainFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        newModels = new ArrayList<>();
        newModels2 = new ArrayList<>();
        rssService = new RssService();
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Загрузка...");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        titleText = (TextView) view.findViewById(R.id.collaps_title);
        imageView = (ImageView) view.findViewById(R.id.image);
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        loadNews(recyclerView, imageView);
        return view;
    }

    private int getColumnCount(){
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        DisplayMetrics metricsB = new DisplayMetrics();
        display.getMetrics(metricsB);
        return metricsB.widthPixels > 400 ? 2 : 1;
    }

    public void updateNews(){
        newModels.clear();
        loadNews(recyclerView, imageView);
    }

    private void loadNews(final RecyclerView recyclerView, final ImageView imageView){
        progressDialog.show();
        final GridLayoutManager manager = new GridLayoutManager(getActivity(), getColumnCount() );
        recyclerView.setLayoutManager(manager);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (adapter.isFooter(position)){
                    return manager.getSpanCount();
                }else return 1;
            }
        });
        Action1<List<NewModel>> onNextAction = new Action1<List<NewModel>>() {
            @Override
            public void call(List<NewModel> s) {
                progressDialog.hide();
                if (s != null && s.size() != 0) {
                    Picasso.with(getActivity())
                            .load(s.get(0).imageLink)
                            .placeholder(R.drawable.no_image)
                            .into(imageView);
                    titleText.setText(s.get(0).title);
                    newModels = s;
                    s.remove(0);
                    newModels2.add(newModels.get(0));
                    newModels2.add(newModels.get(1));
                    newModels2.add(newModels.get(2));
                    newModels2.add(newModels.get(3));
                    adapter = new NewsAdapter(s);
                    recyclerView.setAdapter(adapter);
                } else {
                    Toast.makeText(getActivity(), "Ошибка на сервере", Toast.LENGTH_SHORT).show();
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                Toast.makeText(getContext(), "Ошибка подключения к сети", Toast.LENGTH_LONG).show();
                return null;
            }
        };

        if (newModels.size() == 0){
            rssService.getNews(onNextAction, error);
        }else{
            Picasso.with(getActivity())
                    .load(newModels.get(0).imageLink)
                    .into(imageView);
            adapter = new NewsAdapter(newModels);
            recyclerView.setAdapter(adapter);
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_update) {
            updateNews();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
