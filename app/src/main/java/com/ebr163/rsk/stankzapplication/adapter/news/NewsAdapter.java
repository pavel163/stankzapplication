package com.ebr163.rsk.stankzapplication.adapter.news;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebr163.rsk.stankzapplication.R;
import com.ebr163.rsk.stankzapplication.model.NewModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Bakht on 17.11.2015.
 */
public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_FOOTER = 0;
    private static final int TYPE_ITEM_CH = 2;
    private static final int TYPE_ITEM_NOCH = 1;

    public static List<NewModel> data;

    public NewsAdapter(List<NewModel> data){
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        if(viewType == TYPE_FOOTER) {
            View v = LayoutInflater.from (parent.getContext ()).inflate (R.layout.footer_layout, parent, false);
            return FooterViewHolder.newInstance(v, 0);
        }else if (viewType == TYPE_ITEM_CH){
            View view = LayoutInflater.from(context).inflate(R.layout.item_new_ch, parent, false);
            return NewsViewHolder.newInstance(view);
        }else {
            View view = LayoutInflater.from(context).inflate(R.layout.item_new_noch, parent, false);
            return NewsViewHolder.newInstance(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (isFooter(position)) {
            FooterViewHolder holder = (FooterViewHolder) viewHolder;
            holder.title1.setText(data.get(0).title);
            holder.title2.setText(data.get(1).title);
            holder.title3.setText(data.get(2).title);
            holder.title4.setText(data.get(3).title);

            holder.image1.setTag(data.get(0).imageLink);
            Picasso.with( holder.image1.getContext())
                    .load(data.get(0).imageLink)
                    .placeholder(R.drawable.no_image)
                    .into(holder.image1);
            holder.image2.setTag(data.get(1).imageLink);
            Picasso.with( holder.image2.getContext())
                    .load(data.get(1).imageLink)
                    .placeholder(R.drawable.no_image)
                    .into(holder.image2);
            holder.image3.setTag(data.get(2).imageLink);
            Picasso.with( holder.image3.getContext())
                    .load(data.get(2).imageLink)
                    .placeholder(R.drawable.no_image)
                    .into(holder.image3);
            holder.image4.setTag(data.get(3).imageLink);
            Picasso.with( holder.image4.getContext())
                    .load(data.get(3).imageLink)
                    .placeholder(R.drawable.no_image)
                    .into(holder.image4);

            holder.pubdata1.setText(data.get(0).date);
            holder.pubdata2.setText(data.get(1).date);
            holder.pubdata3.setText(data.get(2).date);
            holder.pubdata4.setText(data.get(3).date);
        }else {
            NewsViewHolder holder = (NewsViewHolder) viewHolder;
            holder.setTitle(data.get(position).title);
            holder.setDescription(data.get(position).description);
            holder.setImage(data.get(position).imageLink);
            holder.setLink(data.get(position).link);
            holder.setH2(data.get(position).h2);
            holder.setPubdata(data.get(position).date);
            holder.setContent(data.get(position).content);
        }
    }


    public boolean isHeader(int position) {
        return position == 0;
    }

    public boolean isFooter(int position) {
        return position == data.size() - 1;
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public int getItemViewType (int position) {
        if(isFooter (position)) {
            return TYPE_FOOTER;
        } else if (position%2 != 0) {
            return TYPE_ITEM_NOCH;
        }else {
            return TYPE_ITEM_CH;
        }
    }
}
