package com.ebr163.rsk.stankzapplication.adapter.news;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebr163.rsk.stankzapplication.R;
import com.ebr163.rsk.stankzapplication.adapter.exclusive.ExclusiveAdapter;
import com.ebr163.rsk.stankzapplication.details.DetailActivity;
import com.ebr163.rsk.stankzapplication.model.NewModel;
import com.twitter.sdk.android.tweetcomposer.Card;

import java.util.List;

/**
 * Это УГ, а не код, но для быстрой сдачи приходиться гадить.
 */
public class FooterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public CardView cardView1;
    public CardView cardView2;
    public CardView cardView3;
    public CardView cardView4;

    public TextView title1;
    public ImageView image1;
    public TextView link1;
    public TextView pubdata1;

    public TextView title2;
    public ImageView image2;
    public TextView link2;
    public TextView pubdata2;

    public TextView title3;
    public ImageView image3;
    public TextView link3;
    public TextView pubdata3;

    public TextView title4;
    public ImageView image4;
    public TextView link4;
    public TextView pubdata4;

    private int type;

    public FooterViewHolder(View itemView,
                            CardView cardView1, CardView cardView2, CardView cardView3, CardView cardView4,
                            TextView title1, ImageView image1, TextView link1, TextView pubdata1,
                            TextView title2, ImageView image2, TextView link2, TextView pubdata2,
                            TextView title3, ImageView image3, TextView link3, TextView pubdata3,
                            TextView title4, ImageView image4, TextView link4, TextView pubdata4,
                            int type) {
        super(itemView);

        this.type = type;

        this.cardView1 = cardView1;
        this.cardView1.setOnClickListener(this);
        this.cardView2 = cardView2;
        this.cardView2.setOnClickListener(this);
        this.cardView3 = cardView3;
        this.cardView3.setOnClickListener(this);
        this.cardView4 = cardView4;
        this.cardView4.setOnClickListener(this);

        this.image1 = image1;
        this.image2 = image2;
        this.image3 = image3;
        this.image4 = image4;

        this.title1 = title1;
        this.title2 = title2;
        this.title3 = title3;
        this.title4 = title4;

        this.link1 = link1;
        this.link2 = link2;
        this.link3 = link3;
        this.link4 = link4;

        this.pubdata1 = pubdata1;
        this.pubdata2 = pubdata2;
        this.pubdata3 = pubdata3;
        this.pubdata4 = pubdata4;
    }

    public static FooterViewHolder newInstance(View parent, int type) {
        CardView cardView1 = (CardView) parent.findViewById(R.id.card1);
        CardView cardView2 = (CardView) parent.findViewById(R.id.card2);
        CardView cardView3 = (CardView) parent.findViewById(R.id.card3);
        CardView cardView4 = (CardView) parent.findViewById(R.id.card4);

        TextView title1 = (TextView) parent.findViewById(R.id.title1);
        TextView title2 = (TextView) parent.findViewById(R.id.title2);
        TextView title3 = (TextView) parent.findViewById(R.id.title3);
        TextView title4 = (TextView) parent.findViewById(R.id.title4);

        ImageView image1 = (ImageView) parent.findViewById(R.id.image1);
        ImageView image2 = (ImageView) parent.findViewById(R.id.image2);
        ImageView image3 = (ImageView) parent.findViewById(R.id.image3);
        ImageView image4 = (ImageView) parent.findViewById(R.id.image4);

        TextView link1 = (TextView) parent.findViewById(R.id.link1);
        TextView link2 = (TextView) parent.findViewById(R.id.link2);
        TextView link3 = (TextView) parent.findViewById(R.id.link3);
        TextView link4 = (TextView) parent.findViewById(R.id.link4);

        TextView pubdata1 = (TextView) parent.findViewById(R.id.pubdata1);
        TextView pubdata2 = (TextView) parent.findViewById(R.id.pubdata2);
        TextView pubdata3 = (TextView) parent.findViewById(R.id.pubdata3);
        TextView pubdata4 = (TextView) parent.findViewById(R.id.pubdata4);

        return new FooterViewHolder(parent, cardView1, cardView2, cardView3, cardView4,
                title1, image1, link1, pubdata1,
                title2, image2, link2, pubdata2,
                title3, image3, link3, pubdata3,
                title4, image4, link4, pubdata4,
                type);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(v.getContext(), DetailActivity.class);
        List<NewModel> data = type == 0 ? NewsAdapter.data : ExclusiveAdapter.data;
        switch (v.getId()) {
            case R.id.card1:
                intent.putExtra("news", data.get(0));
                intent.putExtra("news1", data.get(1));
                intent.putExtra("news2", data.get(2));
                intent.putExtra("news3", data.get(3));
                intent.putExtra("news4", data.get(4));
                v.getContext().startActivity(intent);
                break;
            case R.id.card2:
                intent.putExtra("news", data.get(1));
                intent.putExtra("news1", data.get(0));
                intent.putExtra("news2", data.get(2));
                intent.putExtra("news3", data.get(3));
                intent.putExtra("news4", data.get(4));
                v.getContext().startActivity(intent);
                break;
            case R.id.card3:
                intent.putExtra("news", data.get(2));
                intent.putExtra("news1", data.get(0));
                intent.putExtra("news2", data.get(1));
                intent.putExtra("news3", data.get(3));
                intent.putExtra("news4", data.get(4));
                v.getContext().startActivity(intent);
                break;
            case R.id.card4:
                intent.putExtra("news", data.get(3));
                intent.putExtra("news1", data.get(0));
                intent.putExtra("news2", data.get(1));
                intent.putExtra("news3", data.get(2));
                intent.putExtra("news4", data.get(4));
                v.getContext().startActivity(intent);
                break;
        }
    }
}
