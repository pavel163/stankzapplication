package com.ebr163.rsk.stankzapplication.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ebr163.rsk.stankzapplication.R;
import com.ebr163.rsk.stankzapplication.RssService;
import com.ebr163.rsk.stankzapplication.adapter.exclusive.ExclusiveAdapter;
import com.ebr163.rsk.stankzapplication.base.BaseFragment;
import com.ebr163.rsk.stankzapplication.model.NewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by Bakht on 27.11.2015.
 */
public class ExclusiveFragment extends BaseFragment {

    private ExclusiveAdapter adapter;
    private RecyclerView recyclerView;
    private ImageView imageView;
    private TextView titleText;
    private RssService rssService;
    private List<NewModel> newModels;

    public static ExclusiveFragment newInstance(){
        ExclusiveFragment exclusiveFragment = new ExclusiveFragment();
        return exclusiveFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        newModels = new ArrayList<>();
        rssService = RssService.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        titleText = (TextView) view.findViewById(R.id.collaps_title);
        imageView = (ImageView) view.findViewById(R.id.image);
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        loadNews(recyclerView, imageView);
        return view;
    }

    public void updateNews(){
        newModels.clear();
        loadNews(recyclerView, imageView);
    }

    private void loadNews(final RecyclerView recyclerView, final ImageView imageView){
        final GridLayoutManager manager = new GridLayoutManager(getActivity(), 1 );
        recyclerView.setLayoutManager(manager);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return adapter.isHeader(position) ? manager.getSpanCount() : 1;
            }
        });
        Action1<List<NewModel>> onNextAction = new Action1<List<NewModel>>() {
            @Override
            public void call(List<NewModel> s) {
                if (s != null && s.size() != 0) {
                    Picasso.with(getActivity())
                            .load(s.get(0).imageLink)
                            .placeholder(R.drawable.no_image)
                            .into(imageView);
                    titleText.setText(s.get(0).title);
                    newModels = s;
                    s.remove(0);
                    adapter = new ExclusiveAdapter(s);
                    recyclerView.setAdapter(adapter);
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                Toast.makeText(getContext(), "Ошибка подключения к сети", Toast.LENGTH_LONG).show();
                return null;
            }
        };

        if (newModels.size() == 0){
            rssService.getExclusive(onNextAction, error);
        }else{
            Picasso.with(getActivity())
                    .load(newModels.get(0).imageLink)
                    .into(imageView);
            adapter = new ExclusiveAdapter(newModels);
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_update) {
            updateNews();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        newModels.clear();
    }
}
