package com.ebr163.rsk.stankzapplication;

import android.util.Log;

import com.ebr163.rsk.stankzapplication.model.NewModel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Bakht on 21.11.2015.
 */
public class RssService {

    private static final String ITEM = "item";
    private static final String TITLE = "title";
    private static final String LINK = "link";
    private static final String DESCRIPTION = "description";
    private static final String IMG = "src=\"";

    private String htmlNews = "http://stan.kz/feed/";
    private String htmlExclusive = "http://stan.kz/category/exclusive/feed/";
    private List<NewModel> news;
    private List<NewModel> newsExc;

    Document doc = null;

    private static RssService instance;

    public static RssService getInstance() {
        if (instance == null) {
            synchronized (RssService.class) {
                if (instance == null) {
                    instance = new RssService();
                }
            }
        }
        return instance;
    }

    public RssService() {
        news = new ArrayList<>();
        newsExc = new ArrayList<>();
    }

    public List<NewModel> syncNews() {
        try {
            doc = Jsoup.connect(String.valueOf(new URL(htmlNews)))
                    .ignoreHttpErrors(true)
                    .timeout(10000)
                    .get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        doc = Jsoup.parse(doc.toString(), "", Parser.xmlParser());
            for (Element element : doc.select(ITEM)) {
            NewModel newModel = new NewModel();
            newModel.title = element.getElementsByTag(TITLE).get(0).text();
            newModel.link = element.getElementsByTag(LINK).get(0).text();

            String description = element.getElementsByTag(DESCRIPTION).get(0).text();
            if (description.contains("<a")){
                newModel.description = description.substring(0, description.indexOf("<a"));
            }else {
                newModel.description = description;
            }
            String image = newModel.description;
            newModel.description = RssHelper.clearText(newModel.description.substring(newModel.description.lastIndexOf("<div>") + 5));
            if(image.contains("img")){
                image = image.substring(image.indexOf("img"));
                if (image.contains("jpg")){
                    image = image.substring(image.indexOf("http"), image.indexOf("jpg")+3);
                }else if(image.contains("png")){
                    image = image.substring(image.indexOf("http"), image.indexOf("png")+3);
                }else if(image.contains("gif")){
                    image = image.substring(image.indexOf("http"), image.indexOf("gif")+3);
                } if (image.contains("jpeg")) {
                    image = image.substring(image.indexOf("http"), image.indexOf("jpeg")+4);
                }
            }
            newModel.imageLink = image;
            newModel.content = getContent(element.getElementsByTag("content:encoded").get(0));
            newModel.h2 = getH2(element.getElementsByTag("content:encoded").get(0));
            newModel.date = RssHelper.reformatDataandTime(element.getElementsByTag("pubdate").get(0).text());
            news.add(newModel);
        }
        return news;
    }

    public List<NewModel> syncExc() {
        try {
            doc = Jsoup.connect(String.valueOf(new URL(htmlExclusive)))
                    .ignoreHttpErrors(true)
                    .timeout(10000)
                    .get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        doc = Jsoup.parse(doc.toString(), "", Parser.xmlParser());
        for (Element element : doc.select(ITEM)) {
            NewModel newModel = new NewModel();
            newModel.title = element.getElementsByTag(TITLE).get(0).text();
            newModel.link = element.getElementsByTag(LINK).get(0).text();

            String description = element.getElementsByTag(DESCRIPTION).get(0).text();
            if (description.contains("<a")){
                newModel.description = description.substring(0, description.indexOf("<a")); //description.substring(0, description.indexOf(A)-1);
            }else {
                newModel.description = description;
            }
            String image = newModel.description;
            newModel.description =RssHelper.clearText(newModel.description.substring(newModel.description.lastIndexOf("<div>") + 5));
            if(image.contains("img")){
                image = image.substring(image.indexOf("img"));
                if (image.contains("jpg")){
                    image = image.substring(image.indexOf("http"), image.indexOf("jpg")+3);
                }else if(image.contains("png")){
                    image = image.substring(image.indexOf("http"), image.indexOf("png")+3);
                }else if(image.contains("gif")){
                    image = image.substring(image.indexOf("http"), image.indexOf("gif")+3);
                }else if (image.contains("jpeg")) {
                    image = image.substring(image.indexOf("http"), image.indexOf("jpeg")+4);
                }
            }
            newModel.imageLink = image;
            newModel.content = getContent(element.getElementsByTag("content:encoded").get(0));
            newModel.h2 = getH2(element.getElementsByTag("content:encoded").get(0));
            newModel.date = RssHelper.reformatDataandTime(element.getElementsByTag("pubdate").get(0).text());
            newsExc.add(newModel);
        }
        return newsExc;
    }

    private void subscribeAction(Action1 action, Func1 error, final int type) {
        Observable<List<NewModel>> myObservable = Observable.create(
                new Observable.OnSubscribe<List<NewModel>>() {
                    @Override
                    public void call(Subscriber<? super List<NewModel>> subscriber) {
                        if (type == 0){
                            subscriber.onNext(syncNews());
                        }else {
                            subscriber.onNext(syncExc());
                        }
                        subscriber.onCompleted();
                    }
                }
        );
        myObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .onErrorResumeNext(error)
                .subscribe(action, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.e("RssService", throwable.toString());
                    }
                });
    }

    public void getNews(Action1 action1, Func1 error) {
        subscribeAction(action1, error, 0);
    }

    public void getExclusive(Action1 action1, Func1 error) {
        subscribeAction(action1, error, 1);
    }

    private String getContent(Element element){
        String content = element.text();
        content = element.getElementsByTag("content:encoded").get(0).text();
        if (content.contains("<h2>")){
            content = content.replace(content.substring(content.indexOf("<h2>") + 4, content.indexOf("</h2>")), "");
        }
        if (content.indexOf("</p>") !=  content.lastIndexOf("</p>")) {
            content = content.substring(content.indexOf("</p>") + 4, content.lastIndexOf("</p>"));
            content = RssHelper.clearImg(content);
            while (content.contains("<p>")) {
                content = content.replace("<p>", "");
            }
            while (content.contains("</p>")) {
                content = content.replace("</p>", "\n\n");
            }
        }
        return RssHelper.clearText(content);
    }

    private String getH2(Element element){
        String content = element.text();
        if (content.contains("<h2>")){
            String h2 = element.getElementsByTag("content:encoded").get(0).text().substring(content.indexOf("<h2>") + 4, content.indexOf("</h2>"));
            return RssHelper.clearText(h2);
        }
        return "";
    }

    public void removeDoc(){
    }
}
