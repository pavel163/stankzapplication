package com.ebr163.rsk.stankzapplication.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Bakht on 16.11.2015.
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
