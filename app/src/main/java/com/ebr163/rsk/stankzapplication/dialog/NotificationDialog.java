package com.ebr163.rsk.stankzapplication.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;

import com.ebr163.rsk.stankzapplication.MainActivity;
import com.ebr163.rsk.stankzapplication.R;

import java.util.Locale;

/**
 * Created by pavel on 07.12.15.
 */
public class NotificationDialog extends DialogFragment implements DialogInterface.OnClickListener {

    private String data[];
    private String lang;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        data = new String[2];
        /*data[0] = getResources().getString(R.string.language_ru);
        data[1] = getResources().getString(R.string.language_kk);*/
        data[0] = "Вкл";
        data[1] = "Выкл";
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Уведомления")
                .setSingleChoiceItems(data, -1, this)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
    }
}

