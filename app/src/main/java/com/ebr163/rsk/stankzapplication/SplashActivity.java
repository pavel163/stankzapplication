package com.ebr163.rsk.stankzapplication;

import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        TextView textView = (TextView) findViewById(R.id.stan_kz);
        SpannableString content = new SpannableString("STAN.KZ");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        textView.setText(content);
      //  SystemClock.sleep(TimeUnit.SECONDS.toMillis(3));
        textView.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, 3000);
     /*   Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();*/

    }
}
