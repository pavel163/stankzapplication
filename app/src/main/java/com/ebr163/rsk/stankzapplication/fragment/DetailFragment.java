package com.ebr163.rsk.stankzapplication.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebr163.rsk.stankzapplication.R;
import com.ebr163.rsk.stankzapplication.base.BaseFragment;
import com.ebr163.rsk.stankzapplication.details.DetailActivity;
import com.ebr163.rsk.stankzapplication.model.NewModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by pavel on 01.12.15.
 */
public class DetailFragment extends BaseFragment {

    private NewModel newModel;
    private ImageView imageNews;
    private TextView title;
    private TextView description;
    private TextView h2;
    private TextView pubdata;

    public TextView title1;
    public ImageView image1;
    public TextView link1;
    public TextView pubdata1;

    public TextView title2;
    public ImageView image2;
    public TextView link2;
    public TextView pubdata2;

    public TextView title3;
    public ImageView image3;
    public TextView link3;
    public TextView pubdata3;

    public TextView title4;
    public ImageView image4;
    public TextView link4;
    public TextView pubdata4;
    private List<NewModel> list;

    private CardView cardView1;
    private CardView cardView2;
    private CardView cardView3;
    private CardView cardView4;

    public static DetailFragment newInstance() {
        DetailFragment detailFragment = new DetailFragment();
        return detailFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        imageNews = (ImageView) view.findViewById(R.id.image_new);
        title = (TextView) view.findViewById(R.id.title_new);
        description = (TextView) view.findViewById(R.id.description_new);
        h2 = (TextView) view.findViewById(R.id.h2_new);
        pubdata = (TextView) view.findViewById(R.id.pubdata);


        cardView1 = (CardView) view.findViewById(R.id.card1);
        cardView2 = (CardView) view.findViewById(R.id.card2);
        cardView3 = (CardView) view.findViewById(R.id.card3);
        cardView4 = (CardView) view.findViewById(R.id.card4);

        cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                intent.putExtra("news", list.get(0));
                if (!checkList(intent)) {
                    intent.putExtra("news1", newModel);
                    intent.putExtra("news2", list.get(1));
                    intent.putExtra("news3", list.get(2));
                    intent.putExtra("news4", list.get(3));
                }
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });

        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                intent.putExtra("news", list.get(1));
                if (!checkList(intent)) {
                    intent.putExtra("news1", list.get(0));
                    intent.putExtra("news2", newModel);
                    intent.putExtra("news3", list.get(2));
                    intent.putExtra("news4", list.get(3));
                }
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });

        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                intent.putExtra("news", list.get(2));
                if (!checkList(intent)) {
                    intent.putExtra("news1", list.get(0));
                    intent.putExtra("news2", list.get(1));
                    intent.putExtra("news3", newModel);
                    intent.putExtra("news4", list.get(3));
                }
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });

        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                intent.putExtra("news", list.get(3));
                if (!checkList(intent)) {
                    intent.putExtra("news1", list.get(0));
                    intent.putExtra("news2", list.get(1));
                    intent.putExtra("news3", list.get(2));
                    intent.putExtra("news4", newModel);
                }
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });

        title1 = (TextView) view.findViewById(R.id.title1);
        title2 = (TextView) view.findViewById(R.id.title2);
        title3 = (TextView) view.findViewById(R.id.title3);
        title4 = (TextView) view.findViewById(R.id.title4);

        image1 = (ImageView) view.findViewById(R.id.image1);
        image2 = (ImageView) view.findViewById(R.id.image2);
        image3 = (ImageView) view.findViewById(R.id.image3);
        image4 = (ImageView) view.findViewById(R.id.image4);

        link1 = (TextView) view.findViewById(R.id.link1);
        link2 = (TextView) view.findViewById(R.id.link2);
        link3 = (TextView) view.findViewById(R.id.link3);
        link4 = (TextView) view.findViewById(R.id.link4);

        pubdata1 = (TextView) view.findViewById(R.id.pubdata1);
        pubdata2 = (TextView) view.findViewById(R.id.pubdata2);
        pubdata3 = (TextView) view.findViewById(R.id.pubdata3);
        pubdata4 = (TextView) view.findViewById(R.id.pubdata4);

        return view;
    }

    private boolean checkList(Intent intent) {
        for (int i = 0; i < list.size(); i++) {
            if (newModel.title.equals(list.get(i).title)) {
                intent.putExtra("news1", list.get(0));
                intent.putExtra("news2", list.get(1));
                intent.putExtra("news3", list.get(2));
                intent.putExtra("news4", list.get(3));
                return true;
            }
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_detail, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_social) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "http:" + newModel.link);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void setNewModel(NewModel newModel) {
        this.newModel = newModel;
        title.setText(newModel.title);
        h2.setText(newModel.h2);
        description.setText(newModel.content);
        pubdata.setText(newModel.date);
        Picasso.with(getContext())
                .load(newModel.imageLink)
                .placeholder(R.drawable.no_image)
                .into(imageNews);

    }

    public void setFooter(List<NewModel> list) {
        this.list = list;

        title1.setText(list.get(0).title);
        title2.setText(list.get(1).title);
        title3.setText(list.get(2).title);
        title4.setText(list.get(3).title);

        Picasso.with(getContext())
                .load(list.get(0).imageLink)
                .placeholder(R.drawable.no_image)
                .into(image1);
        Picasso.with(getContext())
                .load(list.get(1).imageLink)
                .placeholder(R.drawable.no_image)
                .into(image2);
        Picasso.with(getContext())
                .load(list.get(2).imageLink)
                .placeholder(R.drawable.no_image)
                .into(image3);
        Picasso.with(getContext())
                .load(list.get(3).imageLink)
                .placeholder(R.drawable.no_image)
                .into(image4);

        pubdata1.setText(list.get(0).date);
        pubdata2.setText(list.get(1).date);
        pubdata3.setText(list.get(2).date);
        pubdata4.setText(list.get(3).date);
    }
}
