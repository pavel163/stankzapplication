package com.ebr163.rsk.stankzapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.ebr163.rsk.stankzapplication.base.BaseWithFragmentActivity;

public class WebActivity extends BaseWithFragmentActivity {

    public static final String URL = "url";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        initToolbar();

        WebView webView = (WebView) findViewById(R.id.web);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);

        WebSettings webSettings = webView.getSettings();
        webSettings.setSaveFormData(true);
        webSettings.setJavaScriptEnabled(true);

        webView.loadUrl(getIntent().getStringExtra(URL));
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                WebActivity.this.setProgress(progress * 100);

                if (progress == 100){
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        toolbarTitle.setText("STAN.KZ");
    }
}
